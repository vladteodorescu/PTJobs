import * as React from "react"
import {AppViewer} from "pages/AppViewer"
import {Header} from "pages/Header"
import {Footer} from "pages/Footer"
import {BrowserRouter as Router, Link, Route, Redirect} from 'react-router-dom';
import 'style/landing.less';

export class App extends React.Component<{}, {}>
{
    render()
    {

        return(
            <div>
            
            <Router>  
            <div>
            <Header />
            
            <AppViewer/>
            
            
            </div>
            </Router> 
            
            </div>
        );
    }
}