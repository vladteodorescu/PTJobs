import { JobObject } from 'objects/JobObject';


export class JobService {

    static API = "http://localhost:54566/api/job/";

    static headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });

    static getCategories(addjob: any) {

        var categoriess: any;
        categoriess = '';
        var categFinal: any;
        categFinal = [];
        return fetch(this.API, { method: "GET", headers: this.headers })
            .then((response) => response.json())
            .then(function (data) {
                categoriess = data;
            })
            .then(() => {
                categoriess.map((categ: any) => { categFinal.push({ value: categ.category, label: categ.category, id: categ.categoryId }); })
                addjob.setState({ categories: categFinal });

                console.log(categoriess);
            }
            )
            .catch(function (error) {
                addjob.setState({ categories: [] });
            })
    }
    static addJob(page: any, jObj: JobObject) {
        var myheader = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });

        var request = new Request(this.API, {
            method: "POST",
            headers: myheader,
            body: JSON.stringify({
                "CategoryId": jObj.categoryId, "Title": jObj.title, "Text": jObj.text, "Price": jObj.price,
                "StartDate": jObj.startDate, "EndDate": jObj.endDate, "StartHour": jObj.startHour, "EndHour": jObj.endHour,
                "UserId": jObj.userId
            })
        });
        let response = fetch(request).then(function (response) {
            if (response.ok) {
                return response.json();
            }
            else {
                console.log("Response Fail", response);
                page.setState({ errorPost: response });
            }
        })
            .then(() => (page.setState({ success: true })))
            .catch(function (error) {
                console.log("Signup Error", error);
                page.setState({ errorPost: error });
            })
    }

    static getAllJobsFiltered(page: any) {

        var jobs: any;
        jobs = '';
        return fetch(this.API+"?categoryId="+page.state.categSelected.id+"&judet="+page.state.judet.value+"&idPret="+
        page.state.pretId+"&oraS="+page.state.oraS.value+"&oraF="+page.state.oraE.value, { method: "GET", headers: this.headers })
            .then((response) => response.json())
            .then(function (data) {
                jobs = data;
            })
            .then(() => {
                page.setState({ allJobs: jobs });
                console.log(jobs);
            }
            )
            .catch(function (error) {
                page.setState({ categories: [] });
            })
    }
}
