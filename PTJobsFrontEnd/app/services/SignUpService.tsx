import { SignUpObject } from 'objects/SignUpObject';

/*public username : string;
public email : string;
public password : string;
public name : string;
public varsta : number;*/
export class SignUpServices {

    static API = "http://localhost:54566/api/signup/provider";
    static APIclient = "http://localhost:54566/api/signup/client";

    static signUp(signup: any, suObj: SignUpObject) {
        var myheader = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });

        var request = new Request(this.API, {
            method: "POST",
            headers: myheader,
            body: JSON.stringify({ "username": suObj.username, "password": suObj.password, "nume": suObj.nume, "prenume": suObj.prenume, "email": suObj.email, "varsta": suObj.varsta, "telefon": suObj.telefon, linkCV: "", "descriere": "", "avatar": "" })
        });
        let response = fetch(request).then(function (response) {
            if (response.ok) {
                return response.json();
            }
            else {
                console.log("Response Fail", response);
                signup.setState({ registerFailError: true });

            }
        })
            .then(() => (signup.changeRoute()))
            .catch(function (error) {
                console.log("Signup Error", error);
                signup.setState({ registerFailError: true });
            })
    }

    static signUpClient(signup: any, suObj: SignUpObject) {
        var myheader = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });

        var request = new Request(this.APIclient, {
            method: "POST",
            headers: myheader,
            body: JSON.stringify({ "username": suObj.username, "password": suObj.password, "nume": suObj.nume, "prenume": suObj.prenume, "email": suObj.email, "varsta": suObj.varsta, "telefon": suObj.telefon, linkCV: "", "descriere": "", "avatar": "" })
        });
        let response = fetch(request).then(function (response) {
            if (response.ok) {
                return response.json();
            }
            else {
                console.log("Response Fail", response);
                signup.setState({ registerFailError: true });

            }
        })
            .then(() => (signup.changeRoute()))
            .catch(function (error) {
                console.log("Signup Error", error);
                signup.setState({ registerFailError: true });
            })
    }
}   