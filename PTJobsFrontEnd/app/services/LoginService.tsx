import {LoginObject} from 'objects/LogInObject';


export class LoginServices {

    static API = "http://localhost:54566/api/login/";

    static tryToLogIn(login : any,lg : LoginObject) {
        var myheader = new Headers({'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9'});

        var request = new Request(this.API, {
            method: "POST",
            headers: myheader,
            body: JSON.stringify({"username": lg.username, "password": lg.password})
        });


        let response = fetch(request).then(function (response) {
            if (response.ok) {
                return response.json();
            }
            else{
                console.log("here");
                login.loginFailed();
            }
        })
            .then(function (data) {
                    localStorage.setItem("userId", data.UserId);
                    localStorage.setItem("role", data.UserType);
            })
            .then(()=>login.changeRoute())
            .catch(function (error) {
                console.log("here");
                login.loginFailed();
            })    
    }
}   