import { PrivateProfileClientObject } from 'objects/PrivateProfileClientObject';
import { PrivateProfileProviderObject } from 'objects/PrivateProfileProviderObject';


export class ProfileService {

    static API = "http://localhost:54566/api/profile/";
    static APIpost = "http://localhost:54566/api/profile/client";
    static APIpostP = "http://localhost:54566/api/profile/provider";
    static headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });

    static getInfo(profile: any, lg: PrivateProfileClientObject) {

        var user: any;
        user = '';
        return fetch(this.API + "?idClient=" + profile.props.id, { method: "GET", headers: this.headers })
            .then((response) => response.json())
            .then(function (data) {
                user = data;
            })
            .then(() => {
                profile.setState({ username: user.Username, email: user.Email, lname: user.Nume, fname: user.Prenume, varsta: user.Varsta, telefon: user.Telefon, avatarUrl: user.Avatar }),
                    console.log(user);
            }
            )
            .catch(function (error) {
                console.log('request failed! Try again', error)
            })

    }

        static getInfoProvider(profile: any, lg: PrivateProfileProviderObject) {

        var user: any;
        user = '';
        return fetch(this.API + "?idProvider=" + profile.props.id, { method: "GET", headers: this.headers })
            .then((response) => response.json())
            .then(function (data) {
                user = data;
            })
            .then(() => {
                profile.setState({ username: user.Username, email: user.Email, lname: user.Nume, fname: user.Prenume, varsta: user.Varsta, telefon: user.Telefon, avatarUrl: user.Avatar, CV:user.LinkCV, descriere:user.Descriere }),
                    console.log(user);
            }
            )
            .catch(function (error) {
                console.log('request failed! Try again', error)
            })

    }

    static updateInfo(profile: any, obj: PrivateProfileClientObject){
          var request = new Request(this.APIpost, {
            method: "PUT",
            headers: this.headers,
            body: JSON.stringify({Varsta: obj.varsta, Telefon: obj.telefon, Avatar: obj.avatarUrl, Id: obj.id})
        });
        let response = fetch(request).then(function (response) {
            if (response.ok) {
                return response.json();
            }
            else{
               // profile.updateFailed();
            }
        })
            .then(function (data) {
                   profile.setState({success : true});
            })
            .catch(function (error) {
               // peofile.updateFailed();
            })    
    }

     static updateInfoProvider(profile: any, obj: PrivateProfileProviderObject){
          var request = new Request(this.APIpostP, {
            method: "PUT",
            headers: this.headers,
            body: JSON.stringify({Varsta: obj.varsta, Telefon: obj.telefon, Avatar: obj.avatarUrl, LinkCV: obj.CV, descriere:obj.descriere, Id: obj.id})
        });
        let response = fetch(request).then(function (response) {
            if (response.ok) {
                return response.json();
            }
            else{
               // profile.updateFailed();
            }
        })
            .then(function (data) {
                   profile.setState({success : true});
            })
            .catch(function (error) {
               // peofile.updateFailed();
            })    
    }
    
}   