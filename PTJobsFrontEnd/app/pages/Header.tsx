import * as React from "react"
import { BrowserRouter as Router, Link, Route, Redirect } from 'react-router-dom';

export class Header extends React.Component<{}, {}>
{
    constructor() {
        super();
        this.logOut = this.logOut.bind(this);
    }

    logOut() {
        localStorage.setItem("userId", "");
        localStorage.setItem("role", "");
    }

    render() {
        let head = null;
        if (localStorage.getItem("userId") == '') {
            head = <div className="header-landing">
                <div className="container-fluid">
                    <div className="logo navbar-header" >
                        <Link className="navbar-brand" to='/'><img src="/photos/logo.png" /> StudentJobs</Link>
                    </div>


                    <ul className="nav navbar-nav navbar-right white">
                        <li><Link to='/login' className="button-signup-small">Autentificare</Link></li>
                    </ul>
                </div>
            </div>
        }
        else if (localStorage.getItem("role") == 'Client') {
            head =
                <nav className="header">
                    <div className="container-fluid">
                        <div className="logo navbar-header">
                            <Link className="navbar-brand" to='/'><img src="/photos/logo.png" /> StudentJobs</Link>
                        </div>
                        <ul className="nav navbar-nav">
                            <li><Link to='/providers' className=" header-button">Prestatori de servicii</Link></li>
                            <li><Link to='/adauga-job' className=" header-button">Adauga job</Link></li>
                            <li><Link to='/my-jobs' className=" header-button">Joburile mele</Link></li>
                        </ul>

                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to='/profile' className="header-link"><div className="glyphicon glyphicon-user"></div> My Profile</Link></li>
                            <li><Link to="/" onClick={this.logOut} className="header-link">
                                <div className="glyphicon glyphicon-log-out"></div> Log Out
                            </Link></li>

                        </ul>
                    </div>
                </nav >

        }
        else if (localStorage.getItem("role") == 'Provider') {
            head =
                <nav className="header">
                    <div className="container-fluid">
                        <div className="logo navbar-header">
                            <Link className="navbar-brand" to='/'><img src="/photos/logo.png" /> StudentJobs</Link>
                        </div>
                        <ul className="nav navbar-nav">
                            <li><Link to='/jobs' className=" header-button">Gaseste joburi</Link></li>
                            <li><Link to='/my-jobs' className=" header-button">Joburile mele</Link></li>
                        </ul>

                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to='/profile' className="header-link"><div className="glyphicon glyphicon-user"></div> My Profile</Link></li>
                            <li><Link to="/" onClick={this.logOut} className="header-link">
                                <div className="glyphicon glyphicon-log-out"></div> Log Out
                            </Link></li>

                        </ul>
                    </div>
                </nav >
        }
        return (
            <div>
                {head}
            </div>

        );
    }
}