import * as React from "react"
import { Redirect } from 'react-router-dom';

export class Loading extends React.Component<{}, { finished: boolean }>
{
    constructor() {
        super();
        this.state = { finished: false }
    }

    componentDidMount() {
        let that = this;
        setTimeout(function () {
            if (localStorage.getItem("userId") != undefined && localStorage.getItem("userId") != "" && localStorage.getItem("userId") != null) {
                that.setState({ finished: true });
            }
            else {
                that.setState({ finished: false });
            }
        }, 500);
    }

    render() {
        let loader = null;
        if (this.state.finished == true) {
            window.location.replace("/");
        }
        else {
            loader = <div ><img  className="nume" src="/photos/loader.gif" /></div>
        }
        return (
            <div>
                {loader}
            </div>
        );
    }
}