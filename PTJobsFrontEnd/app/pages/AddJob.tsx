import * as React from "react"
import * as DatePicker from "react-bootstrap-date-picker"
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { JobService } from 'services/JobService';
import { JobObject } from "objects/JobObject"

export class AddJob extends React.Component<{ idQuestion: number }, { categSelected: any, categories: any, 
    oraS: number, oraE: number, title: string, text: string, startDate: any, endDate: any, pret: number, 
    success: boolean, errorPost: string }>
{

    constructor() {
        super();
        this.state = { categSelected: '', categories: [], title: '', text: '', pret: -1, startDate: '', 
        success: false, errorPost: '', endDate: '', oraS: -1, oraE: -1 };
        this.handleText = this.handleText.bind(this);
        this.handleTitlu = this.handleTitlu.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePret = this.handleChangePret.bind(this);
        this.handleChangeDateS = this.handleChangeDateS.bind(this);
        this.handleChangeDateE = this.handleChangeDateE.bind(this);
        this.handleChangeOra1 = this.handleChangeOra1.bind(this);
        this.handleChangeOra2 = this.handleChangeOra2.bind(this);
        this.handleChangeCategorie = this.handleChangeCategorie.bind(this);
    }

    handleText(ev: React.FormEvent<HTMLTextAreaElement>) {
        this.setState({ text: ev.currentTarget.value });
    }
    handleTitlu(ev: React.FormEvent<HTMLInputElement>) {
        this.setState({ title: ev.currentTarget.value });
    }
    handleChangePret(ev: React.FormEvent<HTMLInputElement>) {
        this.setState({ pret: Number(ev.currentTarget.value) });
    }
    handleChangeOra1(option: any) {
        this.setState({ oraS: option.value })
    }
    handleChangeOra2(option: any) {
        this.setState({ oraE: option.value })
    }
    handleChangeCategorie(option: any) {
        this.setState({ categSelected: option })
        console.log(this.state.categSelected)
    }
    handleChangeDateS(value: any, formattedvalue: any) {
        this.setState({ startDate: value })
    }
    handleChangeDateE(value: any, formattedvalue: any) {
        this.setState({ endDate: value })
    }
    handleSubmit(event: any) {
        event.preventDefault();
        var jobObj: JobObject = new JobObject();
        jobObj.categoryId = this.state.categSelected.id;
        jobObj.endDate = this.state.endDate;
        jobObj.endHour = this.state.oraE.toString();
        jobObj.price = this.state.pret.toString();
        jobObj.startDate = this.state.startDate;
        jobObj.startHour = this.state.oraS.toString();
        jobObj.text = this.state.text;
        jobObj.title = this.state.title;
        jobObj.userId = Number(localStorage.getItem("userId"));
        var loginResult = JobService.addJob(this, jobObj);
    }

    private hours = [
        { value: '1', label: '1' },
        { value: '2', label: '2' },
        { value: '3', label: '3' },
        { value: '4', label: '4' },
        { value: '5', label: '5' },
        { value: '6', label: '6' },
        { value: '7', label: '7' },
        { value: '8', label: '8' },
        { value: '9', label: '9' },
        { value: '10', label: '10' },
        { value: '11', label: '11' },
        { value: '12', label: '12' },
        { value: '13', label: '13' },
        { value: '14', label: '14' },
        { value: '15', label: '15' },
        { value: '16', label: '16' },
        { value: '17', label: '17' },
        { value: '18', label: '18' },
        { value: '19', label: '19' },
        { value: '20', label: '20' },
        { value: '21', label: '21' },
        { value: '22', label: '22' },
        { value: '23', label: '23' },
        { value: '00', label: '00' },
    ]

    componentDidMount() {
        JobService.getCategories(this);

    }

    render() {
        let message = null;
        if (this.state.success == true) {
            message = <div className="spacting alert success-alert"> <strong>Success!</strong> Jobul a fost adaugat</div>
        }
        else if (this.state.errorPost != '') {
            message = <div className="spacing alert alert-danger alert-container"> {this.state.errorPost}</div>
        }
        return (
            <div className="spacing row">
                <div className="col-md-2"></div>
                <div className="col-md-8">
                    <h4><i>Adauga un job:</i></h4>
                    Titlu:
                <input type="text" name="titlu" className="form-control input" onChange={this.handleTitlu} />
                    Descriere:
                <textarea className="form-control " rows={9} onChange={this.handleText}></textarea>
                    Data inceputului ofertei:
            <DatePicker onChange={this.handleChangeDateS} value={this.state.startDate} />
                    Data sfarsitului ofertei:
            <DatePicker onChange={this.handleChangeDateE} value={this.state.endDate} />
                    Orele desfasurarii jobului:<br />
                    <Select
                        className="select-ora"
                        value={this.state.oraS}
                        onChange={this.handleChangeOra1}
                        options={this.hours}
                    />

                    <Select
                        className="select-ora"
                        onChange={this.handleChangeOra2}
                        value={this.state.oraE}
                        options={this.hours}
                    />
                    <br />
                    Categorie:
                    <Select
                        name="form-field-name"
                        value={this.state.categSelected}
                        onChange={this.handleChangeCategorie}
                        options={this.state.categories}
                    />
                    Suma oferita pe ora (lei):
                    <input type="number" name="pret" className="form-control input" onChange={this.handleChangePret} />
                    <button className="button-blue" onClick={this.handleSubmit}>Trimite</button>
                    {message}
                </div>
            </div>
        );
    }
}