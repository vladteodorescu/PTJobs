import * as React from 'react'
import { BrowserRouter as Router, Link, Route, Redirect } from 'react-router-dom';
import { PrivateProfileClientObject } from 'objects/PrivateProfileClientObject';
import { PrivateProfileProviderObject } from 'objects/PrivateProfileProviderObject';
import { ProfileService } from 'services/ProfileService';

export class PrivateProfileProvider extends React.Component<{ id: string }, { errorPut: string, success: boolean, username: string, avatarUrl: string, fname: string, lname: string, email: string, varsta: number, telefon: string, descriere: string, CV: string }>{

    baseUrl: string = 'http://localhost:54566/api/profile/client';
    headers: Headers;
    constructor() {
        super();
        this.state = { username: '', errorPut: '', fname: '', lname: '', email: '', varsta: 0, telefon: '', avatarUrl: '', success: false, descriere: '', CV: '' };
        this.handleChangeVarsta = this.handleChangeVarsta.bind(this);
        this.handleChangeTelefon = this.handleChangeTelefon.bind(this);
        this.handleChangeCV = this.handleChangeCV.bind(this);
        this.handleChangeDescr = this.handleChangeDescr.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleChangeAvatar = this.handleChangeAvatar.bind(this);
    }

    componentDidMount() {
        var obj = new PrivateProfileProviderObject();
        obj.avatarUrl = this.state.avatarUrl;
        obj.email = this.state.email;
        obj.fname = this.state.fname;
        obj.lname = this.state.lname;
        obj.telefon = this.state.telefon;
        obj.varsta = this.state.varsta;
        obj.CV = this.state.CV;
        obj.descriere = this.state.descriere;
        ProfileService.getInfoProvider(this, obj);
    }

    handleChangeVarsta(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ varsta: Number(event.currentTarget.value) });
    }

    handleChangeTelefon(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ telefon: event.currentTarget.value });
    }

    handleChangeAvatar(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ avatarUrl: event.currentTarget.value });
    }

    handleChangeCV(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ CV: event.currentTarget.value });
    }

    handleChangeDescr(event: React.FormEvent<HTMLTextAreaElement>) {
        this.setState({ descriere: event.currentTarget.value });
    }

    handleSubmit(event: any) {
        event.preventDefault();
        var obj = new PrivateProfileProviderObject();
        obj.telefon = this.state.telefon;
        obj.varsta = this.state.varsta;
        obj.avatarUrl = this.state.avatarUrl;
        obj.id = Number(this.props.id);
        obj.CV = this.state.CV;
        obj.descriere = this.state.descriere;
        ProfileService.updateInfoProvider(this, obj);
    }

    handleDelete(event: any) {
        event.preventDefault();
    }

    render() {
        let message = null;
        if (this.state.success == true) {
            message = <div className="spacting alert success-alert"> <strong>Success!</strong> Modificarile au fost salvate</div>
        }
        else if (this.state.errorPut != '') {
            message = <div className="spacing alert alert-danger alert-container"> {this.state.errorPut}</div>
        }
        if (localStorage.getItem("userId") != '') {
            return (
                <div className="row body-container">
                    <div className="col col-md-2"></div>
                    <div className="col col-md-4">
                        Nume : {this.state.fname} {this.state.lname}<br />
                        Email : {this.state.email}<br />
                        Username : {this.state.username}<br />
                        Telefon : <input type="text" value={this.state.telefon} name="Telefon" className="form-control" onChange={this.handleChangeTelefon} />
                        Varsta : <input type="number" value={this.state.varsta} name="Varsta" className="form-control" onChange={this.handleChangeVarsta} />
                        Link CV : <input type="text" value={this.state.CV} name="CV" className="form-control" onChange={this.handleChangeCV} />
                        Descriere : <textarea value={this.state.descriere} name="descriere" className="form-control" onChange={this.handleChangeDescr} rows={9} />
                        <Link to="/">Schimba parola</Link><br />
                        <button className="button-blue" onClick={this.handleSubmit}>Salveaza modificarile</button>
                        <button className="button-red" onClick={this.handleDelete}>Stergere cont</button>
                        {message}
                    </div>
                    <div className="col col-md-1"></div>
                    <div className="col col-md-2">
                        <img className="img-responsive" src={this.state.avatarUrl} />
                        Image url:<input type="text" value={this.state.avatarUrl} name="Image" className="form-control" onChange={this.handleChangeAvatar} />

                    </div>
                    <div className="col col-md-3"></div>
                </div>
            )
        }
        else {
            return (
                <div> You are not logged in!</div>
            )
        }
    }
}