import * as React from 'react'
import { BrowserRouter as Router, Link, Route, Redirect } from 'react-router-dom';

export class SuccessSignUp extends React.Component<{},{}>{

    render() {
        return (
            <div>
            <h4>
                Signed up successfully!
            </h4>
                <Link to="/login" className="button-blue">Login</Link>
            </div>
        );
    }
}