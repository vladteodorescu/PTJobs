import * as React from "react"
import { BrowserRouter as Router, Link, Route, Redirect, Switch} from 'react-router-dom';
import { LandingMain } from "landing/LandingMain"
import { SignUpFurnizor } from "landing/SignUpFurnizor"
import { SignUpClient } from "landing/SignUpClient"
import { LogIn } from "landing/LogIn"
import { SignUpChoice } from "landing/SignUpChoice"
import { Dashboard } from "pages/Dashboard"
import { Loading } from "pages/Loading"
import { SuccessSignUp } from "pages/SuccessSignUp"
import { AddJob } from "pages/AddJob"
import { AllJobs } from "pages/AllJobs"
import { PrivateProfileClient } from "pages/PrivateProfileClient"
import { PrivateProfileProvider } from "pages/PrivateProfileProvider"
import { withRouter } from 'react-router'


export class AppViewer extends React.Component<{}, {}>
{

    render() {
        if (localStorage.getItem('userId') === null || localStorage.getItem('userId') == undefined || localStorage.getItem('userId') == "") {
            localStorage.setItem('userId', '');
        }
        let currentUserId = localStorage.getItem('userId') || '';
        return (    
            <div>
            <Switch>
                <Route exact path="/" render={() => (
                    (localStorage.getItem("userId") == '') ? (
                        <LandingMain />
                    ) : (
                            <Dashboard />
                        )
                )} />
                <Route path='/signup-furnizor' component={SignUpFurnizor} />
                <Route path='/signup-beneficiar' component={SignUpClient} />
                <Route path='/login' component={LogIn} />
                <Route path='/loading' component={Loading} />
                <Route path='/sign-up' component={SignUpChoice} />
                <Route path='/adauga-job' component={AddJob} />
                <Route path='/jobs' component={AllJobs} />
                <Route path='/profile' component={(props) => 
                                    (localStorage.getItem('role')  == "Client") ? 
                                    (<PrivateProfileClient id={currentUserId} />)
                                      : ( <PrivateProfileProvider id={currentUserId} /> )
                                    } />
                <Route path='/success-signup' component={SuccessSignUp} />
            </Switch>
            </div>
        );
    }
    
}
