import * as React from "react"
import { Link } from 'react-router-dom';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { JobService } from 'services/JobService';

export class AllJobs extends React.Component<{}, {
    judet: any, categSelected: any, categories: any, pretId: number,
    oraS: any, oraE: any, pret: string, allJobs: any
}>
{

    constructor() {
        super();
        this.state = { categSelected: { id: -1 }, allJobs: [], categories: [{value:"-",label:""}], judet: { value: '-' }, oraS: { value: "-" }, oraE: { value: "-" }, pret: '', pretId: -1 }
        this.handleChangeCategorie = this.handleChangeCategorie.bind(this);
        this.handleChangeJudet = this.handleChangeJudet.bind(this);
        this.handleChangeOra1 = this.handleChangeOra1.bind(this);
        this.handleChangeOra2 = this.handleChangeOra2.bind(this);
        this.handleChangePret = this.handleChangePret.bind(this);
    }

    componentDidMount() {
        JobService.getCategories(this);
        this.filter();
    }



    filter() {
        JobService.getAllJobsFiltered(this);
    }

    handleChangeOra1(option: any) {
        this.setState({ oraS: option })
        console.log(this.state.oraS.value)
    }
    handleChangeOra2(option: any) {
        this.setState({ oraE: option })
    }

    handleChangeCategorie(option: any) {
        this.setState({ categSelected: option })
    }

    handleChangeJudet(option: any) {
        this.setState({ judet: option })
    }
    handleChangePret(option: any) {
        this.setState({ pret: option })
        this.setState({ pretId: option.id });
    }

    render() {
        
        return (
            
            <div className="row">
                <div className="col col-md-2"></div>
                <div className="col col-md-2">
                    Categorie:
                    <Select
                        value={this.state.categSelected}
                        onChange={this.handleChangeCategorie}
                        options={this.state.categories}
                        resetValue={{value:"-",label:"-"}}
                    />
                    Judet:
                    <Select
                        value={this.state.judet}
                        onChange={this.handleChangeJudet}
                        options={this.judete}
                        resetValue={{value:"-",label:"-"}}
                    />
                    Intre orele:<br />
                    <Select
                        className="select-ora"
                        value={this.state.oraS}
                        onChange={this.handleChangeOra1}
                        options={this.hours}
                        resetValue={{value:"-",label:"-"}}
                    />

                    <Select
                        className="select-ora"
                        onChange={this.handleChangeOra2}
                        value={this.state.oraE}
                        options={this.hours}
                        resetValue={{value:"-",label:"-"}}
                    />
                    Castig pe ora (lei):
                    <Select
                        onChange={this.handleChangePret}
                        value={this.state.pret}
                        options={this.preturi}
                        resetValue={{value:"-",label:"-"}}
                    />
                    <br />
                </div>
                <div className="col col-md-6">
                    jobs = {JSON.stringify(this.state.allJobs).toString()}
                    <div className="col col-md-2"></div>
                </div>
            </div>
        );
    }

    private judete = [
        { value: "-", label: "-" },
        { value: "Alba", label: "Alba" },
        { value: "Arad", label: "Arad" },
        { value: "Arges", label: "Arges" },
        { value: "Bacau", label: "Bacau" },
        { value: "Bihor", label: "Bihor" },
        { value: "Bistrita Nasaud", label: "Bistrita Nasaud" },
        { value: "Botosani", label: "Botosani" },
        { value: "Brasov", label: "Brasov" },
        { value: "Braila", label: "Braila" },
        { value: "Bucuresti", label: "Bucuresti" },
        { value: "Buzau", label: "Buzau" },
        { value: "Caras Severin", label: "Caras Severin" },
        { value: "Calarasi", label: "Calarasi" },
        { value: "Cluj", label: "Cluj" },
        { value: "Constanta", label: "Constanta" },
        { value: "Covasna", label: "Covasna" },
        { value: "Dambovita", label: "Dambovita" },
        { value: "Dolj", label: "Dolj" },
        { value: "Galati", label: "Galati" },
        { value: "Giurgiu", label: "Giurgiu" },
        { value: "Gorj", label: "Gorj" },
        { value: "Harghita", label: "Harghita" },
        { value: "Hunedoara", label: "Hunedoara" },
        { value: "Ialomita", label: "Ialomita" },
        { value: "Iasi", label: "Iasi" },
        { value: "Ilfov", label: "Ilfov" },
        { value: "Maramures", label: "Maramures" },
        { value: "Mehedinti", label: "Mehedinti" },
        { value: "Mures", label: "Mures" },
        { value: "Neamt", label: "Neamt" },
        { value: "Olt", label: "Olt" },
        { value: "Prahova", label: "Prahova" },
        { value: "Satu Mare", label: "Satu Mare" },
        { value: "Salaj", label: "Salaj" },
        { value: "Sibiu", label: "Sibiu" },
        { value: "Suceava", label: "Suceava" },
        { value: "Teleorman", label: "Teleorman" },
        { value: "Timis", label: "Timis" },
        { value: "Tulcea", label: "Tulcea" },
        { value: "Vaslui", label: "Vaslui" },
        { value: "Valcea", label: "Valcea" },
        { value: "Vrancea", label: "Vrancea" }]
    private hours = [
        { value: '-', label: '-' },
        { value: '1', label: '1' },
        { value: '2', label: '2' },
        { value: '3', label: '3' },
        { value: '4', label: '4' },
        { value: '5', label: '5' },
        { value: '6', label: '6' },
        { value: '7', label: '7' },
        { value: '8', label: '8' },
        { value: '9', label: '9' },
        { value: '10', label: '10' },
        { value: '11', label: '11' },
        { value: '12', label: '12' },
        { value: '13', label: '13' },
        { value: '14', label: '14' },
        { value: '15', label: '15' },
        { value: '16', label: '16' },
        { value: '17', label: '17' },
        { value: '18', label: '18' },
        { value: '19', label: '19' },
        { value: '20', label: '20' },
        { value: '21', label: '21' },
        { value: '22', label: '22' },
        { value: '23', label: '23' },
        { value: '00', label: '00' },
    ]
    private preturi = [
        { id: '-1', value: '-', label: '-' },
        { id: '0', value: '< 30', label: '< 30' },
        { id: '1', value: '30 - 50', label: '30 - 50' },
        { id: '2', value: '50 - 100', label: '50 - 100' },
        { id: '3', value: '> 100', label: '> 100' }]
}