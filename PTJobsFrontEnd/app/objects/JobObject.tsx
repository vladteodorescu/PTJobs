export class JobObject {
    public categoryId: number;
    public title: string;
    public text: string;
    public price: string;
    public startDate: any;
    public endDate: any;
    public startHour: string;
    public endHour: string;
    public userId:number
}