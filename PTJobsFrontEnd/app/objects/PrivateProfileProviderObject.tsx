export class PrivateProfileProviderObject {
    public username: string;
    public fname: string;
    public lname: string;
    public email: string;
    public varsta: number;
    public telefon: string;
    public avatarUrl: string;
    public CV : string;
    public descriere :string;
    public id:number;
}
