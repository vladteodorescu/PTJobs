export class SignUpObject {
    public nume: string;
    public prenume: string;
    public email: string;
    public password: string;
    public username: string;
    public telefon: string;
    public varsta: number;
}
