export class LoginObject {
    public username : string;
    public password : string;
    public verified : boolean;
}