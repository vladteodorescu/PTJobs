import * as React from "react"
import { Link } from "react-router-dom";
import {Footer} from "landing/LandingFooter"

export class LandingMain extends React.Component<{}, {}>
{



    render() {
        return (
            <div>
                <div className="bg">
                </div>
                <div className="middle-text-landing spacing-bottom">
                    Noi ne concentram pe nevoile tale.
                    <div className="smaller-text">
                    Conexiunile ne inspira.
                    </div>
                </div>
                


                <div className="page-body">

                 <div className="spacing-bottom row">
                    <div className="col col-md-2"></div>
                   
                    <div className="col col-md-10 text">
                    <strong>StudentJobs</strong> este o platforma de intermediere job-uri intre persoane fizice. Prin intermediul acesteia vrem sa venim în sprijinul persoanelor care cauta un job specific, de scurta durata, de cele mai multe ori realizabil de către studenți, dar și de alte persoane.
                    Poti alege din categorii precum: babysitting, petsitting, ore de limbi straine, pregatatire pentru discipline scolare, curatenie si multe altele. 
                    <div className="link"><Link to='/about' >Citeste mai mult...</Link></div> 
                    </div>
                    <div className="col col-md-2"></div>
                </div>
                 <div className="row">
                    <div className="col col-md-3"></div>
                    <div className="col col-md-3">
                        <div className="col col-md-12 grey-container">

                            <img className="landing-icon img-responsive" src="/photos/resume.png"/>
                            Gaseste un job de scurta durata care sa corespunda cerintelor tale.<br/>
                            <div className="button-blue spacing-top"><Link to='/signup-furnizor' >Inregistrare ca furnizor</Link></div> 
                        </div>
                    </div>
                    <div className="col col-md-3">
                        <div className="col col-md-12 grey-container">
                        <img className="landing-icon img-responsive" src="/photos/handshake.png"/>
                            Gaseste persoana potrivita pentru indeplinirea unor sarcini.<br/>
                            <div className="button-blue spacing-top"><Link to='/signup-beneficiar' >Inregistrare ca beneficiar</Link></div> 
                        </div>
                        </div>
                    <div className="col col-md-3"></div>
                </div>
                </div>
                <Footer/>
            </div>
        );
    }
}