import * as React from "react"
import { Link } from 'react-router-dom';

export class Footer extends React.Component <{},{}>
{
    render(){
        return (
            <div className="footer">
            <div className="col col-md-8 col-md-offset-2">
            <h4>StudentJobs</h4>
             <Link to='/about'>Despre noi</Link>
             <Link to='/login'>Autentificare</Link>
             <Link to='/sign-up'>Inregistrare</Link>
            </div>
            </div>
        );
    }
}