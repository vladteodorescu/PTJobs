import * as React from "react"

import { LoginServices } from 'services/LoginService';
import { LoginObject } from 'objects/LogInObject';
import { BrowserRouter as Router, Link, Route, Redirect } from 'react-router-dom';

export class LogIn extends React.Component<{}, { username: string, password: string, errorLoging: boolean, successfulLoging: boolean }>
{

    constructor() {
        super();
        this.state = { username: "", password: "", errorLoging: false, successfulLoging: false };
        this.handleLogInUsername = this.handleLogInUsername.bind(this);
        this.handleLogInPassword = this.handleLogInPassword.bind(this);
        this.tryToLog = this.tryToLog.bind(this);
        this.changeRoute = this.changeRoute.bind(this);

    }

    handleLogInUsername(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ username: event.currentTarget.value });
    }
    handleLogInPassword(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ password: event.currentTarget.value });
    }

    changeRoute() {
        this.setState({ successfulLoging: true });
    }

    tryToLog(event: any) {
        event.preventDefault();
        var loginObject = new LoginObject();
        loginObject.password = this.state.password;
        loginObject.username = this.state.username;
        loginObject.verified = true;
        this.setState({ password: loginObject.password });
        this.setState({ username: loginObject.username });
        var loginResult = LoginServices.tryToLogIn(this, loginObject);
    }

    loginFailed() {
        this.setState({ errorLoging: true });
    }


    render() {
        let errorMessage = null;
        if (this.state.errorLoging == true) {
            errorMessage = <div className="alert error-alert">Username sau parola gresite!</div>
        }
        let redirect = null;
        if (this.state.successfulLoging == true) {
            redirect = <Redirect to="/" />
        }
        return (
            <div>
                <div className="bg">
                </div>
                <div className="col col-xs-0 col-sm-4 col-md-4 col-lg-4"></div>
                <div className="login-container col col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <form>
                        <div className="spacing">
                            Nume utilizator:
                                <input type="text" name="username" className="form-control input" placeholder='Introdu  numele de utilizator' onChange={this.handleLogInUsername} />
                        </div>
                        <div className="spacing">
                            Parolă:
                                <input type="password" name="password" className="form-control input" placeholder='Introdu parola' onChange={this.handleLogInPassword} />
                        </div>
                        <br />
                        <div className="col col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <button className="button-blue" onClick={this.tryToLog}>Autentificare</button>
                        </div>


                        <div className=" col col-xs-12 col-sm-12 col-md-3 col-lg-3"><br /></div>
                        <div className="small-text col col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            Nu ai deja cont?
                            <div className="link"><Link to='/sign-up' >Înregistrează-te</Link></div>
                        </div>
                        <br /><br /><br />
                        {errorMessage}
                        {redirect}
                    </form>
                </div>
                <div className="col col-xs-0 col-sm-4 col-md-4 col-lg-4"></div>

            </div>
        );
    }
}