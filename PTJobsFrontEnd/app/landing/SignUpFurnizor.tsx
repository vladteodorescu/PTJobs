import * as React from "react"
import { SignUpServices } from 'services/SignUpService';
import { Link } from "react-router-dom";
import { SignUpObject } from "objects/SignUpObject"

export class SignUpFurnizor extends React.Component<{}, { nume: string, prenume: string, email: string, password: string, username: string, telefon: string, varsta: number, registerFail: boolean, registerFailError: boolean }>
{
    private pw: string;
    private checkPw: string;
    constructor() {
        super();
        this.state = { nume: "", prenume: "", email: "", password: "", username: "", telefon: "", varsta: 0, registerFail: false, registerFailError: false };
    }


    handleSubmit = (event: any) => {
        event.preventDefault();
        if (this.pw != this.checkPw) {
            return; // error message for user
        }


        var suObject: SignUpObject = new SignUpObject();
        suObject.nume = this.state.nume;
        suObject.prenume = this.state.prenume;
        suObject.email = this.state.email;
        suObject.password = this.state.password;
        suObject.username = this.state.username;
        suObject.telefon = this.state.telefon;
        suObject.varsta = this.state.varsta;
        var loginResult = SignUpServices.signUp(this, suObject);
    }
    private handleNume = (ev: React.FormEvent<HTMLInputElement>) => {
        this.setState({ nume: ev.currentTarget.value });
    }
    private handlePrenume = (ev: React.FormEvent<HTMLInputElement>) => {
        this.setState({ prenume: ev.currentTarget.value });
    }
    private handleEmail = (ev: React.FormEvent<HTMLInputElement>) => {
        this.setState({ email: ev.currentTarget.value });
    }
    private handleUsername = (ev: React.FormEvent<HTMLInputElement>) => {
        this.setState({ username: ev.currentTarget.value });
    }
    private handleParola = (ev: React.FormEvent<HTMLInputElement>) => {
        this.pw = ev.currentTarget.value;
        this.setState({ password: ev.currentTarget.value });
    }
    private handleConfirmParola = (ev: React.FormEvent<HTMLInputElement>) => {
        this.checkPw = ev.currentTarget.value;
        if (this.pw != this.checkPw) {
            this.setState({ registerFail: true });
        }
        else{
            this.setState({ registerFail: false });
        }
    }
    private handleTelefon = (ev: React.FormEvent<HTMLInputElement>) => {
        this.setState({ telefon: ev.currentTarget.value });
    }
    private handleVarsta = (ev: React.FormEvent<HTMLInputElement>) => {
        this.setState({ varsta: Number(ev.currentTarget.value) });
    }

    public registerFail() {
        this.setState({ registerFail: true });
    }

    public changeRoute() {
        window.location.replace("/success-signup");
    }

    render() {
        var errorMsg = null;
        if (this.state.registerFail == true) {
            errorMsg = <div className="alert error-alert">Parolele nu coincid</div>
        }
        if (this.state.registerFailError == true) {
            errorMsg = <div className="alert error-alert">Eroare la sign up</div>
        }
        return (
            <div>
                <div className="bg-fixed">
                </div>
                <div className="col col-xs-0 col-sm-4 col-md-4 col-lg-4"></div>
                <div className="signup-container col col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div className="spacing">Înregistrare ca furnizor de servicii:<br /><br /></div>
                    <form>
                        <div className="spacing">
                            Nume:
                                <input type="text" name="name" className="form-control input" onChange={this.handleNume} />
                        </div>
                        <div className="spacing">
                            Prenume:
                                <input type="text" name="surname" className="form-control input" onChange={this.handlePrenume} />
                        </div>
                        <div className="spacing">
                            E-mail:
                                <input type="text" name="email" className="form-control input" onChange={this.handleEmail} />
                        </div>
                        <div className="spacing">
                            Nume Utilizator:
                                <input type="text" name="username" className="form-control input" onChange={this.handleUsername} />
                        </div>
                        <div className="spacing">
                            Parolă:
                                <input type="password" name="password" className="form-control input" onChange={this.handleParola} />
                        </div>
                        <div className="spacing">
                            Confirmă Parola:
                                <input type="password" name="password" className="form-control input" onChange={this.handleConfirmParola} />
                        </div>
                        <div className="spacing">
                            Telefon:
                                <input type="text" name="phone" className="form-control input" onChange={this.handleTelefon} />
                        </div>
                        <div className="spacing">
                            Varsta:
                                <input type="number" name="age" className="form-control input" onChange={this.handleVarsta} />
                        </div>
                        <br />
                        <button className="button-blue spacing" onClick={this.handleSubmit}>Înregistrare</button>
                        <br /><br />
                    </form>
                    {errorMsg}
                </div>
                <div className="col col-xs-0 col-sm-4 col-md-4 col-lg-4"></div>
            </div>
        );
    }
}