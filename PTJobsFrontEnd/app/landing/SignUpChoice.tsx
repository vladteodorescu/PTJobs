import * as React from "react"
import { Link } from "react-router-dom";
import {Footer} from "landing/LandingFooter"

export class SignUpChoice extends React.Component<{}, {}>
{

    render() {
        return (
            <div>
                <div className="bg">
                </div>
                <div className="signup-choice-container">

                 <div className="row">
                    <div className="col col-md-3"></div>
                    <div className="col col-md-3">
                        <div className="col col-md-12 grey-container">

                            <img className="landing-icon img-responsive" src="/photos/resume.png"/>
                            Gaseste un job de scurta durata care sa corespunda cerintelor tale.<br/>
                            <div className="button-blue spacing-top"><Link to='/signup-furnizor' >Inregistrare ca furnizor</Link></div> 
                        </div>
                    </div>
                    <div className="col col-md-3">
                        <div className="col col-md-12 grey-container">
                        <img className="landing-icon img-responsive" src="/photos/handshake.png"/>
                            Gaseste persoana potrivita pentru indeplinirea unor joburi.<br/>
                            <div className="button-blue spacing-top"><Link to='/signup-beneficiar' >Inregistrare ca beneficiar</Link></div> 
                        </div>
                        </div>
                    <div className="col col-md-3"></div>
                </div>
                </div>
            </div>
        );
    }
}