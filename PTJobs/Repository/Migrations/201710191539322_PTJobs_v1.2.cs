namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PTJobs_v12 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "PortofoliuId", "dbo.Portofolius");
            DropIndex("dbo.Users", new[] { "PortofoliuId" });
            AddColumn("dbo.Portofolius", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Portofolius", "UserId");
            AddForeignKey("dbo.Portofolius", "UserId", "dbo.Users", "Id");
            DropColumn("dbo.Users", "PortofoliuId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "PortofoliuId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Portofolius", "UserId", "dbo.Users");
            DropIndex("dbo.Portofolius", new[] { "UserId" });
            DropColumn("dbo.Portofolius", "UserId");
            CreateIndex("dbo.Users", "PortofoliuId");
            AddForeignKey("dbo.Users", "PortofoliuId", "dbo.Portofolius", "Id");
        }
    }
}
