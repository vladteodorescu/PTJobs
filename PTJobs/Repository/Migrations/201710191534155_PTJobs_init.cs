namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PTJobs_init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Anunts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        Type = c.String(),
                        Title = c.String(),
                        Text = c.String(),
                        Pret = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        StartHour = c.String(),
                        EndHour = c.String(),
                        Views = c.Int(nullable: false),
                        Locatie = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserType = c.Int(nullable: false),
                        Nume = c.String(),
                        Prenume = c.String(),
                        Email = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                        Avatar = c.String(),
                        Varsta = c.Int(nullable: false),
                        Telefon = c.String(),
                        Adresa = c.String(),
                        Descriere = c.String(),
                        PortofoliuId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Portofolius", t => t.PortofoliuId)
                .Index(t => t.PortofoliuId);
            
            CreateTable(
                "dbo.Newsletters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NewsletterText = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Portofolius",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CVLink = c.String(),
                        UserId = c.Int(nullable: false),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.UserId)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Recomandations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReceiverEmail = c.String(),
                        RecomandationText = c.String(),
                        UserRecommendedId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReviewText = c.String(),
                        ReviewRating = c.Int(nullable: false),
                        PosterId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.NewsletterUsers",
                c => new
                    {
                        Newsletter_Id = c.Int(nullable: false),
                        User_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Newsletter_Id, t.User_Id })
                .ForeignKey("dbo.Newsletters", t => t.Newsletter_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Newsletter_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "UserId", "dbo.Users");
            DropForeignKey("dbo.Recomandations", "UserId", "dbo.Users");
            DropForeignKey("dbo.Portofolius", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Users", "PortofoliuId", "dbo.Portofolius");
            DropForeignKey("dbo.Portofolius", "UserId", "dbo.Users");
            DropForeignKey("dbo.NewsletterUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.NewsletterUsers", "Newsletter_Id", "dbo.Newsletters");
            DropForeignKey("dbo.Anunts", "UserId", "dbo.Users");
            DropIndex("dbo.NewsletterUsers", new[] { "User_Id" });
            DropIndex("dbo.NewsletterUsers", new[] { "Newsletter_Id" });
            DropIndex("dbo.Reviews", new[] { "UserId" });
            DropIndex("dbo.Recomandations", new[] { "UserId" });
            DropIndex("dbo.Portofolius", new[] { "User_Id" });
            DropIndex("dbo.Portofolius", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "PortofoliuId" });
            DropIndex("dbo.Anunts", new[] { "UserId" });
            DropTable("dbo.NewsletterUsers");
            DropTable("dbo.Reviews");
            DropTable("dbo.Recomandations");
            DropTable("dbo.Portofolius");
            DropTable("dbo.Newsletters");
            DropTable("dbo.Users");
            DropTable("dbo.Anunts");
        }
    }
}
