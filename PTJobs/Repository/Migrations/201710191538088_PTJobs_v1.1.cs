namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PTJobs_v11 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Portofolius", "UserId", "dbo.Users");
            DropForeignKey("dbo.Portofolius", "User_Id", "dbo.Users");
            DropIndex("dbo.Portofolius", new[] { "UserId" });
            DropIndex("dbo.Portofolius", new[] { "User_Id" });
            DropColumn("dbo.Portofolius", "UserId");
            DropColumn("dbo.Portofolius", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Portofolius", "User_Id", c => c.Int());
            AddColumn("dbo.Portofolius", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Portofolius", "User_Id");
            CreateIndex("dbo.Portofolius", "UserId");
            AddForeignKey("dbo.Portofolius", "User_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Portofolius", "UserId", "dbo.Users", "Id");
        }
    }
}
