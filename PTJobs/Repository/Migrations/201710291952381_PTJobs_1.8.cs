namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PTJobs_18 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Recomandations", name: "UserId", newName: "RecommendedUserId");
            RenameColumn(table: "dbo.Reviews", name: "UserId", newName: "ReviewedUserId");
            RenameIndex(table: "dbo.Recomandations", name: "IX_UserId", newName: "IX_RecommendedUserId");
            RenameIndex(table: "dbo.Reviews", name: "IX_UserId", newName: "IX_ReviewedUserId");
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SoliciterId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                        AdressId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Adresses", t => t.AdressId)
                .ForeignKey("dbo.Jobs", t => t.JobId)
                .ForeignKey("dbo.Users", t => t.SoliciterId)
                .Index(t => t.SoliciterId)
                .Index(t => t.JobId)
                .Index(t => t.AdressId);
            
            CreateTable(
                "dbo.Adresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Judet = c.String(),
                        Localitate = c.String(),
                        Strada = c.String(),
                        Cartier = c.String(),
                        Numar = c.Int(nullable: false),
                        Bloc = c.String(),
                        Apartament = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Jobs", "Price", c => c.String());
            AddColumn("dbo.Users", "LinkCV", c => c.String());
            AddColumn("dbo.Recomandations", "SenderId", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "ReviewerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Recomandations", "SenderId");
            CreateIndex("dbo.Reviews", "ReviewerId");
            AddForeignKey("dbo.Recomandations", "SenderId", "dbo.Users", "Id");
            AddForeignKey("dbo.Reviews", "ReviewerId", "dbo.Users", "Id");
            DropColumn("dbo.Jobs", "Type");
            DropColumn("dbo.Jobs", "Pret");
            DropColumn("dbo.Jobs", "Locatie");
            DropColumn("dbo.Users", "Adresa");
            DropColumn("dbo.Users", "CVLink");
            DropColumn("dbo.Recomandations", "UserRecommendedId");
            DropColumn("dbo.Reviews", "PosterId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reviews", "PosterId", c => c.Int(nullable: false));
            AddColumn("dbo.Recomandations", "UserRecommendedId", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "CVLink", c => c.String());
            AddColumn("dbo.Users", "Adresa", c => c.String());
            AddColumn("dbo.Jobs", "Locatie", c => c.String());
            AddColumn("dbo.Jobs", "Pret", c => c.String());
            AddColumn("dbo.Jobs", "Type", c => c.String());
            DropForeignKey("dbo.Reviews", "ReviewerId", "dbo.Users");
            DropForeignKey("dbo.Requests", "SoliciterId", "dbo.Users");
            DropForeignKey("dbo.Recomandations", "SenderId", "dbo.Users");
            DropForeignKey("dbo.Requests", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.Requests", "AdressId", "dbo.Adresses");
            DropIndex("dbo.Reviews", new[] { "ReviewerId" });
            DropIndex("dbo.Recomandations", new[] { "SenderId" });
            DropIndex("dbo.Requests", new[] { "AdressId" });
            DropIndex("dbo.Requests", new[] { "JobId" });
            DropIndex("dbo.Requests", new[] { "SoliciterId" });
            DropColumn("dbo.Reviews", "ReviewerId");
            DropColumn("dbo.Recomandations", "SenderId");
            DropColumn("dbo.Users", "LinkCV");
            DropColumn("dbo.Jobs", "Price");
            DropTable("dbo.Adresses");
            DropTable("dbo.Requests");
            RenameIndex(table: "dbo.Reviews", name: "IX_ReviewedUserId", newName: "IX_UserId");
            RenameIndex(table: "dbo.Recomandations", name: "IX_RecommendedUserId", newName: "IX_UserId");
            RenameColumn(table: "dbo.Reviews", name: "ReviewedUserId", newName: "UserId");
            RenameColumn(table: "dbo.Recomandations", name: "RecommendedUserId", newName: "UserId");
        }
    }
}
