namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_judet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jobs", "Judet", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Jobs", "Judet");
        }
    }
}
