// <auto-generated />
namespace Repository.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PTJobs_v11 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PTJobs_v11));
        
        string IMigrationMetadata.Id
        {
            get { return "201710191538088_PTJobs_v1.1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
