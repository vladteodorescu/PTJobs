namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PTJobs16 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Anunts", newName: "Jobs");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Jobs", newName: "Anunts");
        }
    }
}
