namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PTJobs_v13 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Portofolius", "UserId", "dbo.Users");
            DropIndex("dbo.Portofolius", new[] { "UserId" });
            AddColumn("dbo.Users", "CVLink", c => c.String());
            DropTable("dbo.Portofolius");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Portofolius",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CVLink = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Users", "CVLink");
            CreateIndex("dbo.Portofolius", "UserId");
            AddForeignKey("dbo.Portofolius", "UserId", "dbo.Users", "Id");
        }
    }
}
