﻿using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Newsletter> Newsletters { get; set; }
        public DbSet<Recomandation> Recomandations { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Request> Requests { get; set; }

        public DatabaseContext() : base("name = DatabaseContext")
        {
            this.Configuration.AutoDetectChangesEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Recomandation>()
                    .HasRequired(m => m.Sender)
                    .WithMany(t => t.RecomandationsGivenByUser)
                    .HasForeignKey(m => m.SenderId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Recomandation>()
                    .HasRequired(m => m.RecommendedUser)
                    .WithMany(t => t.RecomandationsGivenAboutUser)
                    .HasForeignKey(m => m.RecommendedUserId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Review>()
                    .HasRequired(m => m.Reviewer)
                    .WithMany(t => t.ReviewsGivenByUser)
                    .HasForeignKey(m => m.ReviewerId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Review>()
                    .HasRequired(m => m.ReviewedUser)
                    .WithMany(t => t.ReviewsGivenAboutUser)
                    .HasForeignKey(m => m.ReviewedUserId)
                    .WillCascadeOnDelete(false);
        }
    }
}
