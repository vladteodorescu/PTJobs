﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Recomandation :Idable
    {
        public string ReceiverEmail { get; set; }
        public string RecomandationText { get; set; }

        public int SenderId { get; set; }
        public virtual User Sender { get; set; }

        public int RecommendedUserId { get; set; }
        public virtual User RecommendedUser { get; set; }
    }
}
