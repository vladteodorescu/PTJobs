﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class User : Idable
    {
        public int UserType { get; set; }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Avatar { get; set; }
        public int Varsta { get; set; }
        public string Telefon { get; set; }
        public string Descriere { get; set; }
        public string LinkCV{ get; set; }

        public virtual ICollection<Newsletter> Newsletters { get; set; }
        public virtual ICollection<Recomandation> RecomandationsGivenByUser { get; set; }
        public virtual ICollection<Recomandation> RecomandationsGivenAboutUser { get; set; }
        public virtual ICollection<Review> ReviewsGivenByUser { get; set; }
        public virtual ICollection<Review> ReviewsGivenAboutUser { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<Request> Requests { get; set; }

        public User()
        {
            Newsletters = new HashSet<Newsletter>();
            RecomandationsGivenByUser = new HashSet<Recomandation>();
            RecomandationsGivenAboutUser = new HashSet<Recomandation>();
            ReviewsGivenByUser = new HashSet<Review>();
            ReviewsGivenAboutUser = new HashSet<Review>();
            Jobs = new HashSet<Job>();
            Requests = new HashSet<Request>();
        }
    }
}
