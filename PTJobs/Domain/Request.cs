﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Request : Idable
    {
        public int SoliciterId { get; set; }
        public virtual User Soliciter { get; set; }

        public int JobId { get; set; }
        public virtual Job Job { get; set; }

        public int AdressId { get; set; }
        public virtual Adress Adress { get; set; }
    }
}
