﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum PriceEnum
    {
        lessThan30,
        more30less50,
        more50less100,
        more100
    }
}
