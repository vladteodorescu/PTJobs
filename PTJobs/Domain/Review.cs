﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Review : Idable
    {
        public string ReviewText { get; set; }
        public int ReviewRating { get; set; }

        public int ReviewerId { get; set; } 
        public virtual User Reviewer { get; set; }

        public int ReviewedUserId { get; set; }
        public virtual User ReviewedUser { get; set; }
    }
}
