﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Newsletter : Idable
    {
        public string NewsletterText { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public Newsletter ()
        {
            Users = new HashSet<User>();
        }
    }
}
