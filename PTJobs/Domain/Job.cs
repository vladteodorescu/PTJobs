﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Job  :Idable
    {
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StartHour { get; set; }
        public string EndHour { get; set; }
        public int Views { get; set; }
        public string Judet { get; set; }

        public int UserId { get; set; } 
        public virtual User User { get; set; }

        public virtual ICollection<Request> Requests { get; set; }

        public Job()
        {
            Requests = new HashSet<Request>();
        }
    }
}
