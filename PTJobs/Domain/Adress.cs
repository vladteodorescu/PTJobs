﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Adress:Idable
    {
        public string Judet { get; set; }
        public string Localitate { get; set; }
        public string Strada { get; set; }
        public string Cartier { get; set; }
        public int Numar { get; set; }
        public string Bloc { get; set; }
        public string Apartament { get; set; }

        public virtual ICollection<Request> Requests { get; set; }

        public Adress()
        {
            Requests = new HashSet<Request>();
        }
    }
}
