﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTJobs.Models
{
    public class ClientProfileModelForShowing
    {
        public int Id { get; set; }
        public int UserType { get; set; }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Avatar { get; set; }
        public int Varsta { get; set; }
        public string Telefon { get; set; }
        public string Descriere { get; set; }
        public string LinkCV { get; set; }
    }
}