﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTJobs.Models
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string UserType { get; set; }
        public string nume { get; set; }
        public string prenume { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string avatar { get; set; }
        public int varsta { get; set; }
        public string telefon { get; set; }
        public string descriere { get; set; }
        public string linkCV { get; set; }
    }
}