﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTJobs.Models
{
    public class ProviderModel
    {
        public int Id { get; set; }
        public int UserType = 1;
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Varsta { get; set; }
        public string Telefon { get; set; }
    }
}