﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain;
namespace PTJobs.Models
{
    public class JobModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StartHour { get; set; }
        public string EndHour { get; set; }
        public int Views { get; set; }
        public int UserId { get; set; }
        public string Judet { get; set; }
    }
}