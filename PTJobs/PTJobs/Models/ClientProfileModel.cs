﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTJobs.Models
{
    public class ClientProfileModel
    {
        public int Id { get; set; }
        public string Avatar { get; set; }
        public int Varsta { get; set; }
        public string Telefon { get; set; }
    }
}