﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTJobs.Models
{
    public class ProviderProfileModel
    {
        public int Id { get; set; }
        public string Avatar { get; set; }
        public int Varsta { get; set; }
        public string Telefon { get; set; }
        public string Descriere { get; set; }
        public string LinkCV { get; set; }
    }
}