﻿using PTJobs.Mappers;
using PTJobs.Models;
using PTJobs.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PTJobs.Controllers
{
    public class JobController : ApiController
    {
        //GET Type Request /job - get all jobs for given category
        //if there are no jobs in category it returns NotFound()
        public IHttpActionResult GetAllFilteredJobs(int categoryId, string judet, int idPret, string oraS, string oraF) {
            var service = new JobServices();
            var jobs = service.GetAllFilteredJobs(categoryId, judet, idPret, oraS, oraF);
            if (jobs == null)
                return NotFound();
            return Ok(jobs);
        }
        
        //POST Request /job adds job to repo - need to add exception
        public IHttpActionResult PostJob(JobModel jobModel) {
            var service = new JobServices();
            var response = service.addJob(jobModel);
            return Ok(response);
        }

        public IHttpActionResult GetCategories()
        {
            var service = new JobServices();
            var response = service.GetCategories();
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }

        // DELETE Request /job/5 deletes job from repo
       /* public IHttpActionResult DeketeJob(int id) {
            var service = new JobServices();
            service.deleteJob(id);
            return Ok(id);
        }*/
    }
}