﻿using PTJobs.Models;
using PTJobs.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PTJobs.Controllers
{
    public class ProfileController : ApiController
    {
        [Route("api/profile/client")]
        public IHttpActionResult PutProfileClient(ClientProfileModel newClient)
        {
            var service = new UserService();
            var response = service.EditProfileClient(newClient);
            if (response != null)
                return Ok(response);
            return BadRequest();
        }

        [Route("api/profile/provider")]
        public IHttpActionResult PutProfileProvider(ProviderProfileModel newProvider)
        {
            var service = new UserService();
            var response = service.EditProfileProvider(newProvider);
            if (response != null)
                return Ok(response);
            return BadRequest();
        }

        public IHttpActionResult GetProfileClient (int idClient)
        {
            var service = new UserService();
            var response = service.GetProfileClient(idClient);
            if (response == null)
                return BadRequest();
            return Ok(response);
        }

        public IHttpActionResult GetProfileProvider(int idProvider)
        {
            var service = new UserService();
            var response = service.GetProfileProvider(idProvider);
            if (response == null)
                return BadRequest();
            return Ok(response);
        }
    }
}