﻿using PTJobs.Models;
using PTJobs.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PTJobs.Controllers
{
    public class SignUpController : ApiController
    {
        [Route("api/signup/provider")]
        public IHttpActionResult PostSignUpAsProvider(ProviderModel userModel)
        {
            var service = new UserService();
            var response = service.signUpAsProvider(userModel);
            if (response != null)
                return Ok(response);
            return BadRequest();
        }

        [Route("api/signup/client")]
        public IHttpActionResult PostSignUpAsClient(ClientModel userModel)
        {
            var service = new UserService();
            var response = service.signUpAsClient(userModel);
            if (response != null)
                return Ok(response);
            return BadRequest();
        }
    }
}