using PTJobs.Models;
using PTJobs.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PTJobs.Controllers
{
    public class LoginController : ApiController
    {
        //POST Type Request - returns true if username and password are correct else false
        public IHttpActionResult PostLogIn([FromBody] LogInModel logInModel) {
            var service = new UserService();
            var response = service.LogIn(logInModel);
            if (response != null)
                return Ok(response);
            return BadRequest();
        }

        //GET Type Request - recives an int as user id and returns Id,UserType as object response
        public IHttpActionResult LogOut(int userId) {
            var service = new UserService();
            var response = service.LogOut(userId);
            if (response)
                return Ok();
            return BadRequest();
        }
    }
}