﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Repository;
using PTJobs.Models;
using Domain;
namespace PTJobs.Mappers
{
    public class JobToJobModelMapper
    {
        public static JobModel MapJob(Job job)
        {
            return new JobModel
            {
                Id = job.Id,
                CategoryId = job.CategoryId,
                EndDate = job.EndDate,
                EndHour = job.EndHour,
                Price = job.Price,
                StartDate = job.StartDate,
                StartHour = job.StartHour,
                Text = job.Text,
                Title = job.Title,
                UserId = job.UserId,
                Views = job.Views,
                Judet = job.Judet
            };
        }

        public static Job MapJobModel(JobModel jobModel)
        {
            return new Job
            {
                Id = jobModel.Id,
                CategoryId = jobModel.CategoryId,
                EndDate = jobModel.EndDate,
                EndHour = jobModel.EndHour,
                Price = jobModel.Price,
                StartDate = jobModel.StartDate,
                StartHour = jobModel.StartHour,
                Text = jobModel.Text,
                Title = jobModel.Title,
                UserId = jobModel.UserId,
                Views = jobModel.Views,
                Judet = jobModel.Judet
            };
        }
    }
    
    
}