﻿using Domain;
using PTJobs.Models;
using Repository;
using System;
using PTJobs.Mappers;
namespace PTJobs.Services
{
    public class UserService
    {
        //returns true if the username/password input exists in the database else false
        public UserModel LogIn(LogInModel model)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var userList = userRepository.GetAll();
                foreach (var userInList in userList)
                {
                    if (userInList.Username != null && userInList.Username.Equals(model.Username)
                        && userInList.Password != null && userInList.Password.Equals(model.Password))
                    {
                        UserModel userModel = new UserModel();
                        userModel.UserId = userInList.Id;
                        if (userInList.UserType == 1)
                            userModel.UserType = "Provider";
                        else
                            userModel.UserType = "Client";
                        return userModel;
                    }
                }
            }
            return null;
        }

        public bool findUserByUsername(String username)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var userList = userRepository.GetAll();
                foreach (var userInList in userList)
                {
                    if (userInList.Username != null && userInList.Username.Equals(username))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool LogOut(int userId)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var userList = userRepository.GetAll();
                foreach (var userInList in userList)
                {
                    if (userInList.Id.Equals(userId))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void deleteAccount(String username)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var userList = userRepository.GetAll();
                foreach (var userInList in userList)
                {
                    if (userInList.Username != null && userInList.Username.Equals(username))
                    {
                        userRepository.Remove(userInList.Id);
                    }
                }
            }
        }
        
        public ClientProfileModel EditProfileClient(ClientProfileModel newClient)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var userToUpdate = userRepository.Find(newClient.Id);
                userToUpdate.Avatar = newClient.Avatar;
                userToUpdate.Varsta = newClient.Varsta;
                userToUpdate.Telefon = newClient.Telefon;
                userRepository.Update(userToUpdate);
                unitOfWork.Save();
                return newClient;
            }
        }

        public ProviderProfileModel EditProfileProvider(ProviderProfileModel newProvider)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var userToUpdate = userRepository.Find(newProvider.Id);
                userToUpdate.Avatar = newProvider.Avatar;
                userToUpdate.Varsta = newProvider.Varsta;
                userToUpdate.Telefon = newProvider.Telefon;
                userToUpdate.Descriere = newProvider.Descriere;
                userToUpdate.LinkCV = newProvider.LinkCV;
                userRepository.Update(userToUpdate);
                unitOfWork.Save();
                return newProvider;
            }
        }

        public ClientModel signUpAsClient(ClientModel clientModel)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var userToAdd = new User { Nume = clientModel.Nume, Prenume = clientModel.Prenume, Email = clientModel.Email, Varsta = clientModel.Varsta, Username = clientModel.Username, Password = clientModel.Password, UserType = clientModel.UserType, Telefon = clientModel.Telefon, Avatar="", Descriere="", LinkCV="" };
                var userAdded = userRepository.Add(userToAdd);
                unitOfWork.Save();
                return new ClientModel { Id = userAdded.Id, Email = userAdded.Email, Nume = userAdded.Nume, Password = userAdded.Password, Prenume = userAdded.Prenume, Username = userAdded.Username, UserType = userAdded.UserType, Varsta = userAdded.Varsta, Telefon = userAdded.Telefon };
            }
        }

        public ProviderModel signUpAsProvider(ProviderModel providerModel)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var userToAdd = new User { Nume = providerModel.Nume, Prenume = providerModel.Prenume, Email = providerModel.Email, Varsta = providerModel.Varsta, Username = providerModel.Username, Password = providerModel.Password, UserType = providerModel.UserType, Telefon = providerModel.Telefon, Avatar = "", Descriere = "", LinkCV = "" };
                var userAdded = userRepository.Add(userToAdd);
                unitOfWork.Save();
                return new ProviderModel { Id = userAdded.Id, Email = userAdded.Email, Nume = userAdded.Nume, Password = userAdded.Password, Prenume = userAdded.Prenume, Username = userAdded.Username, UserType = userAdded.UserType, Varsta = userAdded.Varsta, Telefon = userAdded.Telefon };
            }
        }

        public ClientProfileModelForShowing GetProfileClient (int idClient)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var user = userRepository.Find(idClient);
                return new ClientProfileModelForShowing { Id = user.Id, Avatar = user.Avatar, Descriere = user.Descriere, Email = user.Email, LinkCV = user.LinkCV, Nume = user.Nume, Prenume = user.Prenume, Telefon = user.Telefon, Username = user.Username, UserType = user.UserType, Varsta = user.Varsta };
            }
        }

        public ProviderProfileModelForShowing GetProfileProvider(int idProvider)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var userRepository = unitOfWork.GetRepository<User>();
                var user = userRepository.Find(idProvider);
                return new ProviderProfileModelForShowing { Id = user.Id, Avatar = user.Avatar, Descriere = user.Descriere, Email = user.Email, LinkCV = user.LinkCV, Nume = user.Nume, Prenume = user.Prenume, Telefon = user.Telefon, Username = user.Username, UserType = user.UserType, Varsta = user.Varsta };
            }
        }
    }
}