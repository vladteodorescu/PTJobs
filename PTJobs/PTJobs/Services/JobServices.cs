﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain;
using Repository;
using PTJobs.Models;
using PTJobs.Mappers;
using Domain.Enums;

namespace PTJobs.Services
{
    public class JobServices
    {
        public List<JobModel> GetAllFilteredJobs(int categoryId, string judet, int idPret, string oraS, string oraF)
        {
            List<JobModel> listOfJobsFiltered = new List<JobModel>();
            List<JobModel> listOfJobsSorted= new List<JobModel>();
            List<JobModel> listOfJobsToReturn = new List<JobModel>();
            using (var unitOfWork = new UnitOfWork())
            {
                var jobRepository = unitOfWork.GetRepository<Job>();
                var jobList = jobRepository.GetAll();
                foreach (var jobInList in jobList)
                {
                    if (categoryId != -1)
                    {
                        if (jobInList.CategoryId == categoryId)
                        {
                            if(!judet.Equals("-"))
                            {
                                if(jobInList.Judet.Equals(judet))
                                {
                                    if(!oraS.Equals("-"))
                                    {
                                        if(Int32.Parse(jobInList.StartHour) <= Int32.Parse(oraS))
                                        {
                                            if(!oraF.Equals("-"))
                                            {
                                                if(Int32.Parse(jobInList.EndHour) >= Int32.Parse(oraF))
                                                {
                                                    if (idPret != -1)
                                                    {
                                                        switch (idPret)
                                                        {
                                                            case 0:
                                                                {
                                                                    if (Int32.Parse(jobInList.Price) <= 30)
                                                                    {
                                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                        listOfJobsFiltered.Add(jobToAdd);
                                                                    }
                                                                    break;
                                                                }
                                                            case 1:
                                                                {
                                                                    if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                                    {
                                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                        listOfJobsFiltered.Add(jobToAdd);
                                                                    }
                                                                    break;
                                                                }
                                                            case 2:
                                                                {
                                                                    if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                                    {
                                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                        listOfJobsFiltered.Add(jobToAdd);
                                                                    }
                                                                    break;
                                                                }
                                                            case 3:
                                                                {
                                                                    if (Int32.Parse(jobInList.Price) >= 100)
                                                                    {
                                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                        listOfJobsFiltered.Add(jobToAdd);
                                                                    }
                                                                    break;
                                                                }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                        listOfJobsFiltered.Add(jobToAdd);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (idPret != -1)
                                                {
                                                    switch (idPret)
                                                    {
                                                        case 0:
                                                            {
                                                                if (Int32.Parse(jobInList.Price) <= 30)
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 1:
                                                            {
                                                                if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 2:
                                                            {
                                                                if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 3:
                                                            {
                                                                if (Int32.Parse(jobInList.Price) >= 100)
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                    }
                                                }
                                                else
                                                {
                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                    listOfJobsFiltered.Add(jobToAdd);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!oraF.Equals("-"))
                                        {
                                            if (Int32.Parse(jobInList.EndHour) >= Int32.Parse(oraF))
                                            {
                                                if (idPret != -1)
                                                {
                                                    switch (idPret)
                                                    {
                                                        case 0:
                                                            {
                                                                if (Int32.Parse(jobInList.Price) <= 30)
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 1:
                                                            {
                                                                if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 2:
                                                            {
                                                                if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 3:
                                                            {
                                                                if (Int32.Parse(jobInList.Price) >= 100)
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                    }
                                                }
                                                else
                                                {
                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                    listOfJobsFiltered.Add(jobToAdd);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (idPret != -1)
                                            {
                                                switch (idPret)
                                                {
                                                    case 0:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) <= 30)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 1:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 2:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 3:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) >= 100)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                            else
                                            {
                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                listOfJobsFiltered.Add(jobToAdd);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (!oraS.Equals("-"))
                                    {
                                    if (Int32.Parse(jobInList.StartHour) <= Int32.Parse(oraS))
                                    {
                                        if (!oraF.Equals("-"))
                                        {
                                            if (Int32.Parse(jobInList.EndHour) >= Int32.Parse(oraF))
                                            {
                                                if (idPret != -1)
                                                {
                                                    switch (idPret)
                                                    {
                                                        case 0:
                                                            {
                                                                if (Int32.Parse(jobInList.Price) <= 30)
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 1:
                                                            {
                                                                if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 2:
                                                            {
                                                                if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 3:
                                                            {
                                                                if (Int32.Parse(jobInList.Price) >= 100)
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                    }
                                                }
                                                else
                                                {
                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                    listOfJobsFiltered.Add(jobToAdd);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (idPret != -1)
                                            {
                                                switch (idPret)
                                                {
                                                    case 0:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) <= 30)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 1:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 2:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 3:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) >= 100)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                            else
                                            {
                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                listOfJobsFiltered.Add(jobToAdd);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!oraF.Equals("-"))
                                    {
                                        if (Int32.Parse(jobInList.EndHour) >= Int32.Parse(oraF))
                                        {
                                            if (idPret != -1)
                                            {
                                                switch (idPret)
                                                {
                                                    case 0:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) <= 30)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 1:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 2:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 3:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) >= 100)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                            else
                                            {
                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                listOfJobsFiltered.Add(jobToAdd);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (idPret != -1)
                                        {
                                            switch (idPret)
                                            {
                                                case 0:
                                                    {
                                                        if (Int32.Parse(jobInList.Price) <= 30)
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 1:
                                                    {
                                                        if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 2:
                                                    {
                                                        if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 3:
                                                    {
                                                        if (Int32.Parse(jobInList.Price) >= 100)
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                            }
                                        }
                                        else
                                        {
                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                            listOfJobsFiltered.Add(jobToAdd);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!judet.Equals("-"))
                            {
                            if (jobInList.Judet.Equals(judet))
                            {
                                if (!oraS.Equals("-"))
                                    {
                                    if (Int32.Parse(jobInList.StartHour) <= Int32.Parse(oraS))
                                    {
                                        if (!oraF.Equals("-"))
                                        {
                                            if (Int32.Parse(jobInList.EndHour) >= Int32.Parse(oraF))
                                            {
                                                if (idPret != -1)
                                                {
                                                    switch (idPret)
                                                    {
                                                        case 0:
                                                            {
                                                                if (Int32.Parse(jobInList.Price) <= 30)
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 1:
                                                            {
                                                                if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 2:
                                                            {
                                                                if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                        case 3:
                                                            {
                                                                if (Int32.Parse(jobInList.Price) >= 100)
                                                                {
                                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                    listOfJobsFiltered.Add(jobToAdd);
                                                                }
                                                                break;
                                                            }
                                                    }
                                                }
                                                else
                                                {
                                                    var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                    listOfJobsFiltered.Add(jobToAdd);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (idPret != -1)
                                            {
                                                switch (idPret)
                                                {
                                                    case 0:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) <= 30)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 1:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 2:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 3:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) >= 100)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                            else
                                            {
                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                listOfJobsFiltered.Add(jobToAdd);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!oraF.Equals("-"))
                                    {
                                        if (Int32.Parse(jobInList.EndHour) >= Int32.Parse(oraF))
                                        {
                                            if (idPret != -1)
                                            {
                                                switch (idPret)
                                                {
                                                    case 0:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) <= 30)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 1:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 2:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 3:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) >= 100)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                            else
                                            {
                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                listOfJobsFiltered.Add(jobToAdd);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (idPret != -1)
                                        {
                                            switch (idPret)
                                            {
                                                case 0:
                                                    {
                                                        if (Int32.Parse(jobInList.Price) <= 30)
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 1:
                                                    {
                                                        if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 2:
                                                    {
                                                        if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 3:
                                                    {
                                                        if (Int32.Parse(jobInList.Price) >= 100)
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                            }
                                        }
                                        else
                                        {
                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                            listOfJobsFiltered.Add(jobToAdd);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!oraS.Equals("-"))
                            {
                                if (Int32.Parse(jobInList.StartHour) <= Int32.Parse(oraS))
                                {
                                    if (!oraF.Equals("-"))
                                    {
                                        if (Int32.Parse(jobInList.EndHour) >= Int32.Parse(oraF))
                                        {
                                            if (idPret != -1)
                                            {
                                                switch (idPret)
                                                {
                                                    case 0:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) <= 30)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 1:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 2:
                                                        {
                                                            if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                    case 3:
                                                        {
                                                            if (Int32.Parse(jobInList.Price) >= 100)
                                                            {
                                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                                listOfJobsFiltered.Add(jobToAdd);
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                            else
                                            {
                                                var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                listOfJobsFiltered.Add(jobToAdd);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (idPret != -1)
                                        {
                                            switch (idPret)
                                            {
                                                case 0:
                                                    {
                                                        if (Int32.Parse(jobInList.Price) <= 30)
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 1:
                                                    {
                                                        if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 2:
                                                    {
                                                        if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 3:
                                                    {
                                                        if (Int32.Parse(jobInList.Price) >= 100)
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                            }
                                        }
                                        else
                                        {
                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                            listOfJobsFiltered.Add(jobToAdd);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (!oraF.Equals("-"))
                                {
                                    if (Int32.Parse(jobInList.EndHour) >= Int32.Parse(oraF))
                                    {
                                        if (idPret != -1)
                                        {
                                            switch (idPret)
                                            {
                                                case 0:
                                                    {
                                                        if (Int32.Parse(jobInList.Price) <= 30)
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 1:
                                                    {
                                                        if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 2:
                                                    {
                                                        if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                                case 3:
                                                    {
                                                        if (Int32.Parse(jobInList.Price) >= 100)
                                                        {
                                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                            listOfJobsFiltered.Add(jobToAdd);
                                                        }
                                                        break;
                                                    }
                                            }
                                        }
                                        else
                                        {
                                            var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                            listOfJobsFiltered.Add(jobToAdd);
                                        }
                                    }
                                }
                                else
                                {
                                    if (idPret != -1)
                                    {
                                        switch (idPret)
                                        {
                                            case 0:
                                                {
                                                    if (Int32.Parse(jobInList.Price) <= 30)
                                                    {
                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                        listOfJobsFiltered.Add(jobToAdd);
                                                    }
                                                    break;
                                                }
                                            case 1:
                                                {
                                                    if ((Int32.Parse(jobInList.Price) <= 50) && (Int32.Parse(jobInList.Price) >= 30))
                                                    {
                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                        listOfJobsFiltered.Add(jobToAdd);
                                                    }
                                                    break;
                                                }
                                            case 2:
                                                {
                                                    if ((Int32.Parse(jobInList.Price) <= 100) && (Int32.Parse(jobInList.Price) >= 50))
                                                    {
                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                        listOfJobsFiltered.Add(jobToAdd);
                                                    }
                                                    break;
                                                }
                                            case 3:
                                                {
                                                    if (Int32.Parse(jobInList.Price) >= 100)
                                                    {
                                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                                        listOfJobsFiltered.Add(jobToAdd);
                                                    }
                                                    break;
                                                }
                                        }
                                    }
                                    else
                                    {
                                        var jobToAdd = JobToJobModelMapper.MapJob(jobInList);
                                        listOfJobsFiltered.Add(jobToAdd);
                                    }
                                }
                            }
                        }
                    }
                }
                var date = DateTime.Now;
                foreach (var job in listOfJobsFiltered)
                {
                    if(job.EndDate > date)
                    {
                        listOfJobsSorted.Add(job);
                    }
                }
                listOfJobsToReturn = listOfJobsSorted.OrderBy(x => x.EndDate).ToList();
            }
            return listOfJobsToReturn;
        }

        public List<Category> GetCategories()
        {
            List<Category> categoriesToReturn = new List<Category>();
            IEnumerable<CategoryEnum> cats = Enum.GetValues(typeof(CategoryEnum)).Cast<CategoryEnum>();
            int i = 0;
            foreach (var x in cats)
            {
                categoriesToReturn.Add(new Category { category = x.ToString(), categoryId = i });
                i++;
            }
            return categoriesToReturn;
        }

        /*public void deleteJob(int jobID)
        {
            List<Job> listOfJobsInCategory = new List<Job>();
            using (var unitOfWork = new UnitOfWork())
            {
                var jobRepository = unitOfWork.GetRepository<Job>();
                jobRepository.Remove(jobID);

            }
        }*/

        public JobModel addJob(JobModel job)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var jobRepository = unitOfWork.GetRepository<Job>();
                var jobToAdd = JobToJobModelMapper.MapJobModel(job);
                jobRepository.Add(jobToAdd);
                unitOfWork.Save();
                return job;
            }
        }
    }
}